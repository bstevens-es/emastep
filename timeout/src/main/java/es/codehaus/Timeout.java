package es.codehaus;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.s3.event.S3EventNotification;
import com.amazonaws.services.s3.model.S3Object;
import es.codehaus.domain.BatchTaskInfo;
import es.codehaus.facade.EsCodeHausContext;
import es.codehaus.facade.EsCodeHausContextCustomizer;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

@Slf4j
public class Timeout implements RequestHandler<BatchTaskInfo, Void> {
    static final String EXAMPLE_ENV_VARIABLE = "env_key";

    private final EsCodeHausContext ctx;

    // this one is for tests
    Timeout(final EsCodeHausContext ctx) {
        this.ctx = ctx;
    }

    // this one is for regular flow
    public Timeout() {
        ctx = EsCodeHausContextCustomizer.customizeInstance()
                .defaultS3Client()
                .defaultDlqHandling()
                .addEnvironmentVariable(EXAMPLE_ENV_VARIABLE)
                .getInstance();
    }

    @Override
    public Void handleRequest(BatchTaskInfo taskInfo, final Context context) {
        log.info("TaskInfo: {}", ctx.serialize(taskInfo));
        return null;
    }

}