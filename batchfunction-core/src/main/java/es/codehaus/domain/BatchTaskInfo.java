package es.codehaus.domain;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BatchTaskInfo {
    String s3Bucket;
    String fullS3Key;
    String action;
    String cognitoId;
    String requestId;
}
