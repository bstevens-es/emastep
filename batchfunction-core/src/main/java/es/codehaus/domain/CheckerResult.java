package es.codehaus.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class CheckerResult {
    public static final String COMPLETE = "Complete";
    public static final String EXPIRED = "Expired";
    public static final String PROCESSING = "PROCESSING";

    @Setter
    @Getter
    public String status; //Complete, Expired, Processing
    @Setter
    @Getter
    public String errorCode;
    @Setter
    @Getter
    public String errorDescription;


}
