package es.codehaus.domain;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ChunkerStatus {

    public Boolean success;
    public String errorCode;
    public String errorDescription;
}
