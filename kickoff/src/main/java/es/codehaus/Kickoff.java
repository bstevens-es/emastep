package es.codehaus;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.s3.event.S3EventNotification;


import com.amazonaws.services.stepfunctions.AWSStepFunctions;
import com.amazonaws.services.stepfunctions.AWSStepFunctionsClientBuilder;
import com.amazonaws.services.stepfunctions.model.StartExecutionRequest;
import com.amazonaws.services.stepfunctions.model.StartExecutionResult;
import es.codehaus.domain.BatchTaskInfo;
import es.codehaus.facade.EsCodeHausContext;
import es.codehaus.facade.EsCodeHausContextCustomizer;
import lombok.extern.slf4j.Slf4j;
import lombok.val;


@Slf4j
public class Kickoff implements RequestHandler<SQSEvent, Void> {
    public final String STATE_MACHINE_ARN = "state_machine_arn";


    private final EsCodeHausContext ctx;

    // this one is for tests
    Kickoff(final EsCodeHausContext ctx) {
        this.ctx = ctx;
    }

    // this one is for regular flow
    public Kickoff() {
        ctx = EsCodeHausContextCustomizer.customizeInstance()
                .defaultS3Client()
                .defaultDlqHandling()
                .addEnvironmentVariable(STATE_MACHINE_ARN)
                .getInstance();
    }

    @Override
    public Void handleRequest(SQSEvent sqsEvent, final Context context) {

        for (SQSEvent.SQSMessage sqsMessage : sqsEvent.getRecords()) {
            log.debug("ApproximateReceiveCount : {} for s3 event : {}",
                    ctx.getApproximateReceiveCountOfSqsMessage(sqsMessage),
                    sqsMessage.getBody()
            );

            val s3Event = ctx.deserialize(sqsMessage.getBody(), S3Event.class);
            log.debug("s3 event was deserialized, records size : {}", s3Event.getRecords().size());

            for (S3EventNotification.S3EventNotificationRecord notificationRecord : s3Event.getRecords()) {
                String bucket = notificationRecord.getS3().getBucket().getName();
                String key = notificationRecord.getS3().getObject().getKey();
                process(bucket, key);

            }

        }

        return null;
    }

    private void process(String bucket, String key) {

        log.debug("Got S3 event, bucket={}, key={}", bucket, key);
        String arn = ctx.getVariable(STATE_MACHINE_ARN);
        log.debug("Arn for state machine is: {}", arn);

        //todo replace this with a class that can split up the S3 keys
        String[] keybits = key.split("\\/");
        String action = keybits[1];
        String cognitoId = keybits[2];
        String requestId = keybits[3];

        BatchTaskInfo batchTaskInfo = new BatchTaskInfo();
        batchTaskInfo.setS3Bucket(bucket);
        batchTaskInfo.setFullS3Key(key);
        batchTaskInfo.setAction(action);
        batchTaskInfo.setCognitoId(cognitoId);
        batchTaskInfo.setRequestId(requestId);

        String json  = ctx.serialize(batchTaskInfo);
        Regions region = Regions.fromName(System.getenv("AWS_REGION"));
        AWSStepFunctionsClientBuilder builder = AWSStepFunctionsClientBuilder.standard()
                .withRegion(region);
        AWSStepFunctions client = builder.build();

        StartExecutionRequest request = new StartExecutionRequest()
                .withName(requestId) //requestId
                .withStateMachineArn(arn)
                .withInput(json);

        StartExecutionResult result = client.startExecution(request);
        log.info("Result from Step Function execution request: {}", ctx.serialize(result));
    }

}
