# batchfunction QuickStart

Have working AWS credentials for DEV001, either `default` or env var `AWS_PROFILE` points to them

This example doesn't run locally, you must deploy it to AWS

1. `mvn clean package` to build the Maven multi-module project
3. `sam deploy` sam deploy --parameter-overrides SourceBucketName=batchfunction-431036401867-clientbucket

To "un-deploy" a stack from the AWS dev environment, run `aws cloudformation delete-stack --stack-name <stackname>`

# batchfunction Details

This project contains source code and supporting files for a serverless application that you can deploy with the AWS SAM
CLI. It includes the following files and folders:

To kick off the step function, run the following command. Make sure and update the request Id when running, because it is used
as a unique name for the Step Function

aws s3 cp ~/my/random/file.txt s3://batchfunction-431036401867-clientbucket/exam-taker/create/cognitoId.1234/requestId.3456
