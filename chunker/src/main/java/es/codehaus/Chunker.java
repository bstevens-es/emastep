package es.codehaus;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import es.codehaus.domain.ChunkerStatus;
import es.codehaus.domain.BatchTaskInfo;
import es.codehaus.facade.EsCodeHausContext;
import es.codehaus.facade.EsCodeHausContextCustomizer;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class Chunker implements RequestHandler<BatchTaskInfo, ChunkerStatus> {
    private final EsCodeHausContext ctx;

    // this one is for tests
    Chunker(final EsCodeHausContext ctx) {
        this.ctx = ctx;
    }

    // this one is for regular flow
    public Chunker() {
        ctx = EsCodeHausContextCustomizer.customizeInstance()
                .defaultS3Client()
                .defaultDlqHandling()
                .getInstance();
    }

    @Override
    public ChunkerStatus handleRequest(BatchTaskInfo batchTaskInfo, final Context context) {

        String s = (batchTaskInfo !=null)? ctx.serialize(batchTaskInfo) : "";
        log.info("Task info received: {}", s);

        ChunkerStatus response = new ChunkerStatus();
        response.success = true;
        return response;
    }

}