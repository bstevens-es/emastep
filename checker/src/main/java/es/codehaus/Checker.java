package es.codehaus;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import es.codehaus.domain.BatchTaskInfo;
import es.codehaus.domain.CheckerResult;
import es.codehaus.facade.EsCodeHausContext;
import es.codehaus.facade.EsCodeHausContextCustomizer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Checker implements RequestHandler<BatchTaskInfo, CheckerResult> {
    static final String EXAMPLE_ENV_VARIABLE = "env_key";
    private final EsCodeHausContext ctx;

    // this one is for tests
    Checker(final EsCodeHausContext ctx) {
        this.ctx = ctx;
    }

    // this one is for regular flow
    public Checker() {
        ctx = EsCodeHausContextCustomizer.customizeInstance()
                .defaultS3Client()
                .defaultDlqHandling()
                .addEnvironmentVariable(EXAMPLE_ENV_VARIABLE)
                .getInstance();
    }

    @Override
    public CheckerResult handleRequest(final BatchTaskInfo batchTaskInfo, final Context context) {
        log.info("TaskInfo: {}", ctx.serialize(batchTaskInfo));
        CheckerResult checkerResult = new CheckerResult();
        checkerResult.status = CheckerResult.COMPLETE;
        return checkerResult;
    }

}