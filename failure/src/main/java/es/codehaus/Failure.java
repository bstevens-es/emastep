package es.codehaus;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.s3.event.S3EventNotification;
import com.amazonaws.services.s3.model.S3Object;
import es.codehaus.facade.EsCodeHausContext;
import es.codehaus.facade.EsCodeHausContextCustomizer;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

@Slf4j
public class Failure implements RequestHandler<SQSEvent, Void> {
    static final String EXAMPLE_ENV_VARIABLE = "env_key";

    private final EsCodeHausContext ctx;

    // this one is for tests
    Failure(final EsCodeHausContext ctx) {
        this.ctx = ctx;
    }

    // this one is for regular flow
    public Failure() {
        ctx = EsCodeHausContextCustomizer.customizeInstance()
                .defaultS3Client()
                .defaultDlqHandling()
                .addEnvironmentVariable(EXAMPLE_ENV_VARIABLE)
                .getInstance();
    }

    @Override
    public Void handleRequest(final SQSEvent input, final Context context) {
        // For SQS messages not involving S3, use this:
        log.trace("parsed env variable : {}", ctx.getVariable(EXAMPLE_ENV_VARIABLE));
        String example = ctx.getVariable(EXAMPLE_ENV_VARIABLE);
        return null;
    }

}