<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <name>Batch Step Function Demo</name>

    <groupId>com.examsoft</groupId>
    <artifactId>batchfunction</artifactId>
    <version>1.0.0</version>
    <packaging>pom</packaging>

    <modules>
        <module>batchfunction-core</module>
        <module>kickoff</module>
        <module>checker</module>
        <module>chunker</module>
        <module>failure</module>
        <module>aggregator</module>
        <module>timeout</module>
    </modules>

    <properties>
        <revision>1.0.0</revision>

        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>

        <es.codehaus.context.version>4.0.0</es.codehaus.context.version>
        <aws-java-sdk.version>1.11.475</aws-java-sdk.version>
        <aws-lambda-java-core.version>1.2.0</aws-lambda-java-core.version>
        <aws-lambda-java-events.version>2.2.4</aws-lambda-java-events.version>
        <aws-lambda-java-log4j2.version>1.0.0</aws-lambda-java-log4j2.version>
        <aws-xray.version>2.2.1</aws-xray.version>
        <amazon-sqs-java-messaging-lib.version>1.0.8</amazon-sqs-java-messaging-lib.version>
        <commons-io.version>2.6</commons-io.version>
        <commons-text.version>1.6</commons-text.version>
        <httpclient.version>4.5.6</httpclient.version>
        <jackson.version>2.9.6</jackson.version>
        <java-jwt.version>3.2.0</java-jwt.version>
        <junit.version>5.4.0</junit.version>
        <log4j.version>2.11.1</log4j.version>
        <mockito.version>2.24.5</mockito.version>
        <resilience4j.version>0.13.2</resilience4j.version>
        <slf4j-api.version>1.7.26</slf4j-api.version>

        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    </properties>

    <dependencies>
        <!-- https://mvnrepository.com/artifact/com.amazonaws/aws-java-sdk-stepfunctions -->
         <dependency>
               <groupId>com.amazonaws</groupId>
               <artifactId>aws-java-sdk-stepfunctions</artifactId>
               <version>${aws-java-sdk.version}</version>
           </dependency>

           <dependency>
               <groupId>es.codehaus</groupId>
               <artifactId>es.codehaus</artifactId>
               <version>${es.codehaus.context.version}</version>
           </dependency>

           <dependency>
               <groupId>com.amazonaws</groupId>
               <artifactId>aws-xray-recorder-sdk-bom</artifactId>
               <version>${aws-xray.version}</version>
               <type>pom</type>
           </dependency>
           <dependency>
               <groupId>com.amazonaws</groupId>
               <artifactId>aws-xray-recorder-sdk-core</artifactId>
               <version>${aws-xray.version}</version>
           </dependency>
           <dependency>
               <groupId>com.amazonaws</groupId>
               <artifactId>aws-xray-recorder-sdk-apache-http</artifactId>
               <version>${aws-xray.version}</version>
           </dependency>
           <dependency>
               <groupId>com.amazonaws</groupId>
               <artifactId>aws-xray-recorder-sdk-aws-sdk</artifactId>
               <version>${aws-xray.version}</version>
           </dependency>
           <dependency>
               <groupId>com.amazonaws</groupId>
               <artifactId>aws-xray-recorder-sdk-aws-sdk-instrumentor</artifactId>
               <version>${aws-xray.version}</version>
           </dependency>

           <dependency>
               <groupId>com.amazonaws</groupId>
               <artifactId>aws-lambda-java-events</artifactId>
               <version>${aws-lambda-java-events.version}</version>
           </dependency>
           <dependency>
               <groupId>com.amazonaws</groupId>
               <artifactId>aws-lambda-java-core</artifactId>
               <version>${aws-lambda-java-core.version}</version>
           </dependency>
           <dependency>
               <groupId>com.amazonaws</groupId>
               <artifactId>aws-java-sdk-cognitoidp</artifactId>
               <version>${aws-java-sdk.version}</version>
           </dependency>

           <dependency>
               <groupId>com.amazonaws</groupId>
               <artifactId>aws-java-sdk-s3</artifactId>
               <version>${aws-java-sdk.version}</version>
           </dependency>
           <dependency>
               <groupId>com.amazonaws</groupId>
               <artifactId>aws-java-sdk-sns</artifactId>
               <version>${aws-java-sdk.version}</version>
           </dependency>
           <dependency>
               <groupId>com.amazonaws</groupId>
               <artifactId>aws-java-sdk-sqs</artifactId>
               <version>${aws-java-sdk.version}</version>
           </dependency>

           <dependency>
               <groupId>com.amazonaws</groupId>
               <artifactId>aws-lambda-java-log4j2</artifactId>
               <version>${aws-lambda-java-log4j2.version}</version>
           </dependency>
           <dependency>
               <groupId>org.apache.logging.log4j</groupId>
               <artifactId>log4j-core</artifactId>
               <version>${log4j.version}</version>
           </dependency>
           <dependency>
               <groupId>org.apache.logging.log4j</groupId>
               <artifactId>log4j-api</artifactId>
               <version>${log4j.version}</version>
           </dependency>
           <dependency>
               <groupId>org.apache.logging.log4j</groupId>
               <artifactId>log4j-slf4j-impl</artifactId>
               <version>${log4j.version}</version>
           </dependency>
           <dependency>
               <groupId>org.slf4j</groupId>
               <artifactId>slf4j-api</artifactId>
               <version>${slf4j-api.version}</version>
           </dependency>

           <dependency>
               <groupId>commons-io</groupId>
               <artifactId>commons-io</artifactId>
               <version>${commons-io.version}</version>
           </dependency>

           <dependency>
               <groupId>org.apache.httpcomponents</groupId>
               <artifactId>httpclient</artifactId>
               <version>${httpclient.version}</version>
           </dependency>

           <dependency>
               <groupId>com.fasterxml.jackson.core</groupId>
               <artifactId>jackson-core</artifactId>
               <version>${jackson.version}</version>
           </dependency>
           <dependency>
               <groupId>com.fasterxml.jackson.core</groupId>
               <artifactId>jackson-databind</artifactId>
               <version>${jackson.version}</version>
           </dependency>
           <dependency>
               <groupId>com.fasterxml.jackson.core</groupId>
               <artifactId>jackson-annotations</artifactId>
               <version>${jackson.version}</version>
           </dependency>

           <dependency>
               <groupId>com.auth0</groupId>
               <artifactId>java-jwt</artifactId>
               <version>${java-jwt.version}</version>
           </dependency>

           <dependency>
               <groupId>org.glassfish.jersey.core</groupId>
               <artifactId>jersey-client</artifactId>
               <version>2.25.1</version>
           </dependency>

           <dependency>
               <groupId>com.google.guava</groupId>
               <artifactId>guava</artifactId>
               <version>27.0.1-jre</version>
           </dependency>

           <dependency>
               <groupId>org.projectlombok</groupId>
               <artifactId>lombok</artifactId>
               <version>1.18.6</version>
               <scope>provided</scope>
           </dependency>

           <!--io.github.resilience4j-->
        <dependency>
            <groupId>io.github.resilience4j</groupId>
            <artifactId>resilience4j-circuitbreaker</artifactId>
            <version>0.13.2</version>
        </dependency>
        <dependency>
            <groupId>io.github.resilience4j</groupId>
            <artifactId>resilience4j-ratelimiter</artifactId>
            <version>0.13.2</version>
        </dependency>
        <dependency>
            <groupId>io.github.resilience4j</groupId>
            <artifactId>resilience4j-retry</artifactId>
            <version>0.13.2</version>
        </dependency>
        <dependency>
            <groupId>io.github.resilience4j</groupId>
            <artifactId>resilience4j-bulkhead</artifactId>
            <version>0.13.2</version>
        </dependency>
        <dependency>
            <groupId>io.github.resilience4j</groupId>
            <artifactId>resilience4j-cache</artifactId>
            <version>0.13.2</version>
        </dependency>
        <dependency>
            <groupId>io.github.resilience4j</groupId>
            <artifactId>resilience4j-timelimiter</artifactId>
            <version>0.13.2</version>
        </dependency>

        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.vintage</groupId>
            <artifactId>junit-vintage-engine</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <version>3.11.1</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.github.tomakehurst</groupId>
            <artifactId>wiremock</artifactId>
            <version>2.21.0</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <version>${mockito.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-junit-jupiter</artifactId>
            <version>${mockito.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>
</project>
